package com.volkair.firebasepractice.domain;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class Product {

    @Exclude
    private String TAG = Product.class.getSimpleName();

    @Exclude
    private String key;
    private String id;
    private String description;

    @Exclude
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Products");

    /**
     * Constructor to allow Firebase to create a new instance of this product.
     * Attributes are initialized by refelection.
     */
    public Product() {
    }

    /**
     * Constructs a new instance of this product based on existing data in Firebase for the given key.
     * Attributes are initialized by reflection.
     *
     * @param key the key under which the product data is stored in Firebase.
     */
    public Product(String key) throws Exception {
        if (key == null) {
            throw new IllegalArgumentException("Key is a mandatory field");
        }
        this.key = key;
        pullFromFirebase();
    }

    /**
     * Constructs a new instance of this product for the given parameters and stores it as a new product in Firebase.
     *
     * @param id the id for this product
     * @param description the description for this product
     */
    public Product(String id, String description) throws Exception {
        if (id == null || id.trim().isEmpty() || description == null || description.trim().isEmpty()) {
            throw new IllegalArgumentException("Id and description are mandatory fields");
        }
        this.id = id.trim();
        this.description = description.trim();
        pushToFirease();
    }

    /**
     * Pushes this product to Firebase.
     * @return true if successful
     *
     * If this is a new Product (key is null) a new Firebase key will be generated and assigned to this Product
     *
     * Product is validated before being pushed to Firebase.
     * Id and description must be filled.
     *
     * @throws Exception if this Product is not valid
     */
    public boolean pushToFirease() throws Exception {
        if (!isValidProduct()) {
            throw new Exception("This Product is not valid - id and description are mandatory fields");
        }
        if (key == null) {
            // create new Product key in Firebase
            // this is a synchronous api call
            // key is directly usable in code below
            key = reference.push().getKey();
        }
        reference.child(key).setValue(this);
        return true;
    }

    /**
     * Pulls the data for the Product key from Firebase.
     * Data gets reflected into the fields of this Product.
     *
     * @return true if succesfull
     */
    private boolean pullFromFirebase() throws Exception {
        if (key == null) {
            // key unknown - pulling data impossible
            throw new Exception("The Product key is unknown - key msut be known in order to pull data from Firebase");
        }
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Products").child("-Lzci9_mtAhv9mo-AWDw");
        reference.child(key).addListenerForSingleValueEvent(new ValueEventListener() {
        //reference.child(key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, System.currentTimeMillis() + " - onDataChange");
                id = dataSnapshot.child("id").getValue(String.class);
                description = dataSnapshot.child("description").getValue(String.class);
                Log.d(TAG, id);
                Log.d(TAG, description);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
        return true;
    }

    private boolean isValidProduct() {
        return id != null && description != null;
    }

    public String getKey() {
        return key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        if (id == null || id.trim().isEmpty()) {
            throw new IllegalArgumentException("Id is a mandatory field");
        }
        this.id = id.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (description == null || description.trim().isEmpty()) {
            throw new IllegalArgumentException("Description is a mandatory field");
        }
        this.description = description.trim();
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(key, product.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }



}

