package com.volkair.firebasepractice.presentation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.volkair.firebasepractice.R;
import com.volkair.firebasepractice.domain.Product;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();

    private EditText idEditText, keyEditText, descriptionEditText;
    private Button pullProductInDomainButton, pullProductInGUIButton, pushProductButton;

    private Product product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // reflects a Product from Firebase in the Activity - standard 95% of documentation
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Products").child("-Lzci9_mtAhv9mo-AWDw");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // create final user in separate thread
                product = dataSnapshot.getValue(Product.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });



        idEditText = findViewById(R.id.idEditText);
        keyEditText = findViewById(R.id.keyEditText);
        descriptionEditText = findViewById(R.id.descriptionEditText);
        pullProductInDomainButton = findViewById(R.id.pullProductInDomainButton);

        pushProductButton = findViewById(R.id.pushProductButton);

        pullProductInDomainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pullProductButtonClicked();
            }
        });


        pushProductButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushProductButtonClicked();
            }
        });



        /*
        Dont want it this way
         */


        pullProductInGUIButton = findViewById(R.id.pullProductInGUIButton);
        pullProductInGUIButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pullProductViaGUI();
            }
        });

    }

    private void pullProductButtonClicked() {

        // you can test pull with key
        // -Lzci9_mtAhv9mo-AWDw

        try {
            String key = keyEditText.getText().toString();
            product = new Product(key);
            Log.d(TAG, System.currentTimeMillis() + " - product created");
            while (product.getId() == null) {
                Log.d(TAG, System.currentTimeMillis() + " - product id still null");
            };

            idEditText.setText(product.getId());
            descriptionEditText.setText(product.getDescription());

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

    }

    private void pushProductButtonClicked() {

        try {
            String id = idEditText.getText().toString();
            String description = descriptionEditText.getText().toString();
            product = new Product(id, description);
            keyEditText.setText(product.getKey());


        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

    }


    private void pullProductViaGUI() {

        keyEditText.setText((product.getKey()));
        idEditText.setText(product.getId());
        descriptionEditText.setText(product.getDescription());

    }

}
